import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Root from './src/Root';

console.disableYellowBox = true;

AppRegistry.registerComponent(appName, () => Root);
