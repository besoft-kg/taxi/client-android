import React from 'react';
import {Container, Content, Card, Spinner, CardItem, Body} from 'native-base';
import {StatusBar, SafeAreaView, PermissionsAndroid, ImageBackground} from 'react-native';
import storage from './utils/storage';
import BaseComponent from './components/BaseComponent';
import {inject, observer} from 'mobx-react';
import Geolocation from 'react-native-geolocation-service';
import AuthenticatedScreen from './screens/AuthenticatedScreen';
import AuthScreen from './screens/AuthScreen';
import {size} from './utils';

@inject('store') @observer
class App extends BaseComponent {
  constructor(props) {
    super(props);

    Promise.all([
      storage.get('token', null),
      storage.get('client', null),
      storage.get('fcm_token', null),
      storage.get('categories', []),
      storage.get('language', 'ru'),
    ]).then(values => {
      this.store.setDataFromStorage(values);

      //this.checkLastVersion();

      this.getCurrentPosition().then(() => {
      });
      this.appStore.checkAuth();
    });
  }

  getCurrentPosition = async () => {
    const not_granted_permissions = await this.notGrantedLocationPermissions();
    if (not_granted_permissions === null) {
      Geolocation.getCurrentPosition(
        (position) => {
          this.appStore.setLocation(position.coords.latitude, position.coords.longitude);

          // Geolocation.watchPosition((response) => {
          //   //this.appStore.setLocation(response.coords.latitude, response.coords.longitude);
          // }, (error) => {
          //   console.log(error.code, error.message);
          // }, {
          //   enableHighAccuracy: true,
          //   distanceFilter: 1,
          //   showLocationDialog: true,
          //   forceRequestLocation: true,
          //   interval: 3000,
          //   fastestInterval: 3000,
          // });
        },
        (error) => {
          console.log(error.code, error.message);
        },
        {
          enableHighAccuracy: true,
          distanceFilter: 1,
          forceRequestLocation: true,
        },
      );
    } else {
      this.requestLocationPermissions(not_granted_permissions).then(() => {
      });
    }
  };

  notGrantedLocationPermissions = async () => {
    const coarse = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
    const fine = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);

    if (!coarse) {
      return PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION;
    }
    if (!fine) {
      return PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION;
    }

    return null;
  };

  requestLocationPermissions = async (permission) => {
    try {
      const granted = await PermissionsAndroid.request(permission);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        this.getCurrentPosition().then(() => {
        });
      } else {
        alert('error');
      }
    } catch (err) {
      console.log(err);
    }
  };

  render() {
    return (
      <SafeAreaView flex={1}>
        <Container style={{backgroundColor: '#eee'}}>
          {this.appStore.app_is_ready ? (
            <>
              {this.appStore.authenticated ? (
                <>
                  <StatusBar translucent={true} backgroundColor={'rgba(0, 0, 0, 0)'}/>
                  <ImageBackground source={require('./images/bg-taxi.jpg')} style={{
                    flex: 1,
                    resizeMode: 'cover',
                    justifyContent: 'flex-end',
                  }}>
                    <AuthenticatedScreen/>
                  </ImageBackground>
                </>
              ) : (
                <>
                  <StatusBar translucent={true} backgroundColor={'rgba(0, 0, 0, 0)'}/>
                  <ImageBackground source={require('./images/bg-taxi.jpg')} style={{
                    flex: 1,
                    resizeMode: 'cover',
                    justifyContent: 'flex-end',
                    paddingTop: StatusBar.currentHeight,
                  }}>
                    <AuthScreen/>
                  </ImageBackground>
                </>
              )}
            </>
          ) : (
            <>
              <StatusBar translucent={true} backgroundColor={'rgba(0, 0, 0, 0)'}/>
              <ImageBackground source={require('./images/bg-taxi.jpg')} style={{
                flex: 1,
                resizeMode: 'cover',
                justifyContent: 'flex-end',
              }}>
                <Content padder contentContainerStyle={{flex: 1, flexDirection: 'column', justifyContent: 'flex-end'}}>
                  <Card>
                    <CardItem>
                      <Body style={{alignItems: 'center', marginVertical: size(12)}}>
                        <Spinner/>
                      </Body>
                    </CardItem>
                  </Card>
                </Content>
              </ImageBackground>
            </>
          )}
        </Container>
      </SafeAreaView>
    );
  }
}

export default App;
