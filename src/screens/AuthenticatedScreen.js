import BaseScreen from './BaseScreen';
import {View} from 'react-native';
import {API_URL, APP_COLOR, DEBUG_MODE, size} from '../utils';
import {Content, H1, Spinner} from 'native-base';
import React from 'react';
import {inject, observer} from 'mobx-react';
import MainComponent from '../components/MainComponent';
import ActiveOrderComponent from '../components/ActiveOrderComponent';
import Pusher from 'pusher-js/react-native';
import {observable} from 'mobx';

@inject('store') @observer
class AuthenticatedScreen extends BaseScreen {
  pusher = null;
  @observable busy = false;

  constructor(props) {
    super(props);

    Pusher.logToConsole = DEBUG_MODE;

    this.pusher = new Pusher('8c0e90a6896b7c598920', {
      cluster: 'eu',
      authEndpoint: `${API_URL}/broadcasting/auth`,
      auth: {
        headers: {
          'Authorization': `Bearer ${this.appStore.token}`,
        },
      },
    });

    // driver_channel.bind('pusher:subscription_succeeded', (data) => {
    //   this.setValue('network_status', 'connected');
    // });
  }

  render() {
    return (
      <Content padder contentContainerStyle={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>

        <View flexGrow={1} alignItems={'center'} justifyContent={'center'}>
          <H1 style={{
            color: '#fff',
            textShadowColor: APP_COLOR,
            textShadowOffset: {width: 0, height: 0},
            textShadowRadius: 10,
          }}>Besoft</H1>
          <H1 style={{
            fontSize: size(20),
            color: '#fff',
            textShadowColor: APP_COLOR,
            textShadowOffset: {width: 0, height: 0},
            textShadowRadius: 10,
            marginBottom: size(6),
            lineHeight: size(23),
          }}>Такси</H1>
        </View>
        <View>
          {this.busy ? (
            <Spinner />
          ) : (
            <>
              {this.orderStore.active_item ? (
                <ActiveOrderComponent pusher={this.pusher}/>
              ) : (
                <MainComponent busy={this.busy}/>
              )}
            </>
          )}
        </View>
      </Content>
    );
  }
}

export default AuthenticatedScreen;
