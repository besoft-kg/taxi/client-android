import React, {Component} from 'react';
import {action} from "mobx";

class BaseScreen extends Component {
  constructor(props) {
    super(props);

    if ('store' in this.props) {
      this.store = this.props.store;
      this.appStore = this.store.appStore;
      this.driverStore = this.store.driverStore;
      this.categoryStore = this.store.categoryStore;
      this.orderStore = this.store.orderStore;
      this.taximeterStore = this.store.taximeterStore;
      this.clientStore = this.store.clientStore;
    }

    if ('t' in this.props) this.t = this.props.t;
  }

  @action setValue = (name, value) => this[name] = value;
}

export default BaseScreen;
