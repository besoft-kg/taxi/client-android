import BaseScreen from './BaseScreen';
import {View, Image} from 'react-native';
import {APP_COLOR, APP_VERSION_CODE, call, languages, size} from '../utils';
import {Body, Button, Card, CardItem, Content, H1, Input, Item, Label, Text} from 'native-base';
import React from 'react';
import {inject, observer} from 'mobx-react';
import {withTranslation} from 'react-i18next';
import {action, computed, observable} from "mobx";
import PhoneNumber from "../utils/PhoneNumber";
import requester from "../utils/requester";

const flags = {
  ky: require('../images/flags/ky.png'),
  ru: require('../images/flags/ru.png'),
  uz: require('../images/flags/uz.png'),
  en: require('../images/flags/en.png'),
};

@withTranslation('auth')
@inject('store') @observer
class AuthScreen extends BaseScreen {
  @observable phone_number = '';
  @observable status = '';
  @observable full_name = '';
  @observable verification_code = '';
  @observable loading = false;

  constructor(props) {
    super(props);

    this.phone_object = new PhoneNumber();
  }

  @action setPhoneNumber = (v) => {
    this.phone_number = v;
    try {
      this.phone_object.parseFromString(v);
    } catch (e) {

    }
  };

  @action sendCode = () => {
    if (this.loading) {
      return;
    }
    this.checked = true;
    if (!this.phone_is_valid) {
      return;
    }

    this.loading = true;
    requester.post('/client/auth/phone', {
      phone_number: this.phone_object.getE164Format().slice(1),
    }).then((response) => {
      console.log(response);
      this.status = response.status;
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      this.loading = false;
    });
  };

  @action checkCode = () => {
    if (this.loading) {
      return;
    } else if (this.verification_code.length === 0) {
      alert('Тастыктоо кодун жазыңыз!');
      return;
    } else if (this.verification_code.length !== 4) {
      alert('Тастыктоо коду туура эмес!');
      return;
    }

    this.loading = true;

    if (this.status.startsWith('not_exists_')) {

      if (this.full_name.length < 3) {
        alert('error');
        return;
      }

      requester.post('/client/auth/phone/register', {
        phone_number: this.phone_object.getE164Format().slice(1),
        verification_code: this.verification_code,
        fcm_token: '',
        platform: 'android',
        version_code: APP_VERSION_CODE,
        full_name: this.full_name,
      }).then((response) => {

        console.log(response);

        switch (response.status) {
          case 'success':
            this.appStore.makeAuth(response.payload);
            break;
        }

      }).catch(e => {
        console.log(e);
      }).finally(() => {
        this.loading = false;
      });

    } else if (this.status.startsWith('exists_')) {

      requester.post('/client/auth/phone/login', {
        phone_number: this.phone_object.getE164Format().slice(1),
        verification_code: this.verification_code,
        fcm_token: '',
        platform: 'android',
        version_code: APP_VERSION_CODE,
      }).then((response) => {

        console.log(response);

        switch (response.status) {
          case 'success':
            this.appStore.makeAuth(response.payload);
            break;
        }

      }).catch(e => {
        console.log(e);
      }).finally(() => {
        this.loading = false;
      });

    }
  };

  @computed get phone_is_valid() {
    return this.phone_number.length > 0 && this.phone_object && this.phone_object.isValid();
  }

  render() {
    return (
      <Content padder contentContainerStyle={{flex: 1, flexDirection: 'column', justifyContent: 'space-between'}}>
        <View style={{backgroundColor: '#fff'}} flexDirection={'row'}>
          {languages.map(v => (
            <Button full flex={1} flexDirection={'column'} flexGrow={1} small onPress={() => this.appStore.setLanguage(v.name)}
                    bordered={v.name !== this.appStore.language} style={{height: size(20)}}>
              <Image style={{width: 18, marginBottom: size(2)}} source={flags[v.name]}/>
              <Text>{v.name}</Text>
            </Button>
          ))}
        </View>
        <View flexGrow={1} alignItems={'center'} justifyContent={'center'}>
          <H1 style={{
            color: '#fff',
            textShadowColor: APP_COLOR,
            textShadowOffset: {width: 0, height: 0},
            textShadowRadius: 10,
          }}>Besoft</H1>
          <H1 style={{
            color: '#fff',
            textShadowColor: APP_COLOR,
            textShadowOffset: {width: 0, height: 0},
            textShadowRadius: 10,
            fontSize: size(25),
            lineHeight: size(27),
            marginVertical: size(8),
          }}>{this.t('app:taxi')}</H1>
        </View>
        <Card style={{borderRadius: 10}}>
          <CardItem style={{borderRadius: 10}}>
            <Body>
              <Text style={{color: 'grey', marginBottom: size(2)}}>{this.t('please_authorize')}</Text>
              <Item style={{marginBottom: size(2)}} floatingLabel>
                <Label>{this.t('input_label.phone_number')}</Label>
                <Input autoFocus={true} onChangeText={v => this.setPhoneNumber(v)} keyboardType="phone-pad"/>
              </Item>
              {this.status.startsWith('not_exists_') && (<Item style={{marginBottom: size(2)}} floatingLabel>
                <Label>{this.t('input_label.full_name')}</Label>
                <Input onChangeText={v => this.setValue('full_name', v)} autoFocus={true}/>
              </Item>)}
              {['exists_code_is_sent', 'not_exists_code_is_sent', 'exists_code_was_sent', 'not_exists_code_was_sent'].includes(this.status) && (
                <Item floatingLabel>
                  <Label>{this.t('input_label.verification_code')}</Label>
                  <Input
                    autoFocus={this.status.startsWith('exists_')}
                    onChangeText={v => this.setValue('verification_code', v)} keyboardType="numeric"/>
                </Item>
              )}
              <Button
                disabled={this.loading}
                style={{marginTop: size(8)}}
                full
                onPress={() => this.status === '' ? this.sendCode() : this.checkCode()}
              >
                <Text>{['exists_code_is_sent', 'not_exists_code_is_sent', 'exists_code_was_sent', 'not_exists_code_was_sent'].includes(this.status) ? this.t('button_label.sign_in') : this.t('button_label.send_code')}</Text>
              </Button>
            </Body>
          </CardItem>
        </Card>
        <Button full onPress={() => call('+996777127127')}
                style={{marginVertical: size(4)}} light>
          <Text>{this.t('button_label.call_taxi')}</Text>
        </Button>
      </Content>
    );
  }
}

export default AuthScreen;
