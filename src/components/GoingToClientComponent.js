import {inject, observer} from "mobx-react";
import {Linking, View} from 'react-native';
import {
  Text,
  Card,
  CardItem,
  H2,
  Button,
  Icon,
  List,
  ListItem,
  Body,
  Left,
} from 'native-base';
import React from 'react';
import BaseComponent from "./BaseComponent";
import {APP_COLOR, size} from "../utils";
import {withTranslation} from "react-i18next";
import requester from "../utils/requester";

@withTranslation('order')
@inject('store') @observer
class GoingToClientComponent extends BaseComponent {
  call = (tel) => {
    Linking.openURL(`tel:+${tel}`);
  };

  cancelOrder = () => {
    requester.post('/client/order/cancel', {
      id: this.orderStore.active_item.id
    }).then((response) => {
      console.log(response);
      switch (response.status) {
        case 'success':
          this.orderStore.createOrUpdate(response.payload);
          break;
      }
    }).catch(e => {
      console.log(e);
    });
  }

  render() {
    const item = this.orderStore.active_item;

    return (
      <View>
        <Card transparent>
          <CardItem style={{backgroundColor: 'rgba(255,255,255,0.89)', borderRadius: size(4)}}>
            <Body>
              <H2 style={{paddingVertical: size(2)}}>
                <Icon type={'MaterialCommunityIcons'} name={'map-marker'}/> {item.address}
              </H2>
            </Body>
          </CardItem>
        </Card>
        <List style={{
          backgroundColor: 'rgba(255,255,255,0.89)',
          marginVertical: size(8),
          borderRadius: size(3)
        }}>
          <ListItem icon style={{marginVertical: size(2)}}>
            <Left>
              <Button style={{ backgroundColor: APP_COLOR }}>
                <Icon name={'account'} type={'MaterialCommunityIcons'} />
              </Button>
            </Left>
            <Body>
              <Text style={{color: APP_COLOR}}>{item.executor.full_name}</Text>
              <Text note>{this.t('label.drivers_full_name')}</Text>
            </Body>
          </ListItem>
          <ListItem icon style={{marginVertical: size(2)}}>
            <Left>
              <Button style={{ backgroundColor: APP_COLOR }}>
                <Icon name={'car'} type={'MaterialCommunityIcons'} />
              </Button>
            </Left>
            <Body>
              <Text style={{color: APP_COLOR}}>{item.executor.state_reg_plate}</Text>
              <Text note>{this.t('label.vehicle_state_reg_plate')}</Text>
            </Body>
          </ListItem>
          <View alignSelf={'center'} style={{margin: size(4)}}>
            <Button iconLeft onPress={() => this.call(item.executor.phone_number)} rounded>
              <Icon type={'MaterialIcons'} name={'phone'}/>
              <Text>
                {this.t('button_label.call_driver')}
              </Text>
            </Button>
          </View>
        </List>
        <Button iconLeft onPress={() => this.call('996556551999')} style={{marginBottom: size(2)}} rounded block>
          <Icon type={'MaterialIcons'} name={'phone'}/>
          <Text>
            {this.t('button_label.call_operator')}
          </Text>
        </Button>
        <Button iconLeft onPress={() => this.cancelOrder()} rounded block danger>
          <Icon type={'MaterialIcons'} name={'cancel'}/>
          <Text>
            {this.t('button_label.cancel_order')}
          </Text>
        </Button>
      </View>
    );
  }
}

export default GoingToClientComponent;
