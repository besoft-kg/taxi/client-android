import React from 'react';
import BaseComponent from './BaseComponent';
import {inject, observer} from 'mobx-react';
import {observable} from 'mobx';
import {View, Alert} from 'react-native';
import {Text} from 'native-base';
import PendingComponent from "./PendingComponent";
import GoingToClientComponent from "./GoingToClientComponent";
import {getSnapshot} from "mobx-state-tree";
import {withTranslation} from "react-i18next";
import InPlaceComponent from "./InPlaceComponent";
import GoingWithClientComponent from "./GoingWithClientComponent";
import CompleteClientComponent from "./CompleteClientComponent";


@withTranslation('order')
@inject('store') @observer
class ActiveOrderComponent extends BaseComponent {
  @observable busy = false;

  componentDidMount() {
    const order_channel = this.props.pusher.subscribe(`private-client.${this.appStore.user.id}.order.${this.orderStore.active_item.id}`);

    order_channel.bind('on-update', (data) => {
      const old_item = getSnapshot(this.orderStore.items.get(data.item.id));
      this.orderStore.createOrUpdate(data.item);
      const item = this.orderStore.active_item;

      if (old_item.status !== item.status) {
        switch (item.status) {
          case 'pending':
            Alert.alert('', `${this.t('alert.cancelled_by_driver.message')}`, [{}], {cancelable: true});
            break;
        }
      }
    });
  }

  render() {
    const item = this.orderStore.active_item;

    if (this.busy) {
      return (
        <View flex={1} justifyContent={'center'}>
          <Text>Loading</Text>
        </View>
      );
    }

    let component = null;

    const props = {
      busy: this.busy,
      onBusy: () => this.setValue('busy', true),
      onFree: () => this.setValue('busy', false),
    };

    switch (item.status) {
      case 'pending':
        component = (
          <PendingComponent {...props} />
        );
        break;
      case 'going_to_client':
        component = (
          <GoingToClientComponent {...props} />
        );
        break;
      case 'in_place':
        component = (
          <InPlaceComponent {...props} />
        );
        break;
      case 'going_with_client':
        component = (
          <GoingWithClientComponent {...props} />
        );
        break;
      case 'completed':
        component = (
          <CompleteClientComponent {...props} />
        );
        break;
    }

    return component;
  }
}

export default ActiveOrderComponent;
