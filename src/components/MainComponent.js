import {View, FlatList, TouchableOpacity, Image} from 'react-native';
import {size} from '../utils';
import {H2, Button, Text, Card, CardItem, Body, Icon} from 'native-base';
import React from 'react';
import {withTranslation} from "react-i18next";
import {inject, observer, Observer} from "mobx-react";
import {observable, values} from "mobx";
import requester from "../utils/requester";
import BaseComponent from "./BaseComponent";

const category_images = {
  1: 'https://www.jcrcab.com/wp-content/uploads/2020/02/jodhpur-taxi.jpg',
  2: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSFfXGoMKSzxRb3ivFztqwGqGbAqXfVuyM-GQ&usqp=CAU',
  3: 'https://www.airportcars-uk.com/wp-content/uploads/2015/09/audi.jpg',
  4: 'https://kia.go.na/wp-content/uploads/2019/05/k2500_new.png',
};

@withTranslation('order')
@inject('store') @observer
class MainComponent extends BaseComponent {
  @observable category_id = 1;

  call = () => {
    if (this.props.busy) return;

    this.setValue('busy', true);
    requester.post('/client/order', {
      address_lat: this.appStore.location_lat,
      address_lng: this.appStore.location_lng,
      address: this.appStore.address,
      category_id: this.category_id,
    }).then((response) => {
      console.log(response);

      switch (response.status) {
        case 'success':
          this.orderStore.createOrUpdate(response.payload);
          break;
      }
    }).catch(e => {
      console.log(e);
    }).finally(() => {
      this.setValue('busy', false);
    });
  };

  renderItem = ({item}) => (
    <Observer>
      {() => (
        <TouchableOpacity
          onPress={() => this.setValue('category_id', item.id)}
          style={{
            backgroundColor: this.category_id === item.id ? 'rgba(41,94,185,0.77)' : '#fff',
            padding: size(2),
            marginBottom: size(4),
            marginRight: size(4),
            borderRadius: 8,
          }}>
          <View alignItems={'center'}>
            <Image style={{width: size(50), borderRadius: 8, height: size(25), marginBottom: size(2)}}
                   source={{uri: `${category_images[item.id]}`}}/>
            <Text flexDirection={'row'} style={{color: 'black', fontSize: size(5)}}>{item.title}</Text>
            <Text flexDirection={'row'} style={{color: '#B63326', fontSize: size(5)}}>{item.landing_price}c</Text>
          </View>
        </TouchableOpacity>
      )}
    </Observer>
  );

  render() {
    return (
      <View>
        <Card alignItems={'center'} transparent>
          <CardItem style={{backgroundColor: 'rgba(255, 255, 255, 0.9)'}}>
            <Body>
              <FlatList
                data={values(this.categoryStore.items)}
                horizontal={true}
                renderItem={this.renderItem}
              />
              <Text style={{color: 'grey', marginTop: size(2), fontWeight: 'bold'}}>{this.t('label.address')}:</Text>
              <H2 style={{paddingVertical: size(3)}}><Icon name={'map-marker'} type={'MaterialCommunityIcons'}/> {this.appStore.address}</H2>
              <Button iconLeft style={{marginTop: size(2)}} onPress={() => this.call()} full rounded>
                <Icon name={'airplane-takeoff'}  type={'MaterialCommunityIcons'} />
                <Text style={{fontSize: size(8), textAlign: 'center', color: '#fff'}}>{this.t('button_label.call_taxi')}</Text>
              </Button>
            </Body>
          </CardItem>
        </Card>
        <Card transparent>
          <CardItem style={{backgroundColor: 'rgba(255, 255, 255, 0.8)'}}>
            <Body>
              <Text>{this.t('label.call_taxi_using_phone')}</Text>
              <Button iconLeft small bordered style={{marginTop: size(4)}} full rounded>
                <Icon name={'phone'} type={'MaterialIcons'} />
                <Text>{this.t('button_label.call_taxi_using_phone')}</Text>
              </Button>
            </Body>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default MainComponent;
