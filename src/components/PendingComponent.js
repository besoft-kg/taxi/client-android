import {Linking, View} from 'react-native';
import {Text, Card, CardItem, Body, H3, Spinner, Button, Icon} from 'native-base';
import React from 'react';
import BaseComponent from "./BaseComponent";
import {size} from "../utils";
import {withTranslation} from "react-i18next";
import {inject, observer} from "mobx-react";
import requester from "../utils/requester";

@withTranslation('order')
@inject('store') @observer
class PendingComponent extends BaseComponent {
  callOperator = () => {
    Linking.openURL(`tel:+${996556551999}`);
  };

  cancelOrder = () => {
    requester.post('/client/order/cancel', {
      id: this.orderStore.active_item.id
    }).then((response) => {
      console.log(response);
      switch (response.status) {
        case 'success':
          this.orderStore.createOrUpdate(response.payload);
          break;
      }
    }).catch(e => {
      console.log(e);
    });
  }

  render() {
    const item = this.orderStore.active_item;

    return (
      <View flexGrow={1} justifyContent={'space-between'}>
        <Card transparent>
          <CardItem style={{backgroundColor: 'rgba(255,255,255,0.89)', borderRadius: size(8)}}>
            <Body>
              <H3 style={{
                textAlign: 'center',
                marginBottom: size(2),
                paddingVertical: size(2),
              }}><Icon type={'MaterialCommunityIcons'}
                       name={'map-marker'}/> {item.address}</H3>
              <View flexGrow={1} alignSelf={'center'}>
                <Text style={{color:'grey'}}>
                  {this.t('label.looking_for_taxi')}
                </Text>
                <Spinner color='blue'/>
              </View>
            </Body>
          </CardItem>
        </Card>
        <View>
          <Button iconLeft onPress={() => this.callOperator()} style={{marginBottom: size(2)}} rounded pull block>
            <Icon type={'MaterialIcons'} name={'phone'}/>
            <Text>
              {this.t('button_label.call_operator')}
            </Text>
          </Button>
          <Button iconLeft onPress={() => this.cancelOrder()} rounded pull block danger>
            <Icon type={'MaterialIcons'} name={'cancel'}/>
            <Text>
              {this.t('button_label.cancel_order')}
            </Text>
          </Button>
        </View>
      </View>
    );
  }
}

export default PendingComponent;
