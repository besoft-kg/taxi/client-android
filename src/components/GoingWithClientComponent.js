import {inject, observer} from "mobx-react";
import {Linking, View} from 'react-native';
import {
  Text,
  Card,
  CardItem,
  Button,
  Icon,
  Body,
} from 'native-base';
import React from 'react';
import BaseComponent from "./BaseComponent";
import {size} from "../utils";
import {withTranslation} from "react-i18next";

@withTranslation('order')
@inject('store') @observer
class GoingWithClientComponent extends BaseComponent {
  call = (tel) => {
    Linking.openURL(`tel:+${tel}`);
  };

  render() {
    const item = this.orderStore.active_item;

    return (
      <View>
        <Card transparent>
          <CardItem style={{backgroundColor: 'rgba(255,255,255,0.89)', marginBottom: size(8), borderRadius: size(4)}}>
            <Body alignItems={'center'} alignSelf={'center'}>
             {/*<video/>*/}
            </Body>
          </CardItem>
        </Card>
        <Button iconLeft onPress={() => this.call('102')} style={{marginBottom: size(2)}} rounded light block>
          <Icon style={{color: '#fff'}} type={'MaterialIcons'} name={'phone'}/>
          <Text style={{color: '#fff'}}>
            {this.t('button_label.call_miliciya')}
          </Text>
        </Button>
        <Button iconLeft onPress={() => this.call('103')} rounded light block>
          <Icon style={{color: '#fff'}} type={'MaterialIcons'} name={'phone'}/>
          <Text style={{color: '#fff'}}>
            {this.t('button_label.call.skoraya')}
          </Text>
        </Button>
      </View>
    );
  }
}

export default GoingWithClientComponent;
