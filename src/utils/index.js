import {PixelRatio, Linking} from 'react-native';

export const APP_NAME = 'Besoft Такси';
export const APP_COLOR = '#01c5c4';

export const DEBUG_MODE = process.env.NODE_ENV !== "production";
export const API_VERSION = 1;
export const APP_VERSION_CODE = 1;
export const APP_VERSION_NAME = '0.1';
export const API_URL = 'https://api.taxi.besoft.kg';

export const size = (dp) => PixelRatio.getPixelSizeForLayoutSize(dp);

export const languages = [
  {name: 'ky', title: 'Кыргызча'},
  {name: 'ru', title: 'Русский'},
  {name: 'uz', title: 'Узбекча'},
  {name: 'en', title: 'English'},
];

export const call = (tel) => Linking.openURL(`tel:${tel}`);

export const buildFormData = (formData, data, parentKey) => {
  if (data && typeof data === 'object' && !(data instanceof Date)) {
    Object.keys(data).forEach(key => {
      buildFormData(formData, data[key], parentKey ? `${parentKey}[${key}]` : key);
    });
  } else {
    const value = data == null ? '' : data;
    formData.append(parentKey, value);
  }
};

export const jsonToFormData = (data) => {
  const formData = new FormData();
  buildFormData(formData, data);
  return formData;
};
