import i18next from 'i18next';
import {DEBUG_MODE} from './index';
import AsyncStoragePlugin from 'i18next-react-native-async-storage';
import storage from './storage';

const detectUserLanguage = (callback) => {
  storage.get('language', 'ru').then(lng => {
    console.log(lng);
    callback(lng);
  }).catch(e => callback('ru'));
};

i18next
  .use(AsyncStoragePlugin(detectUserLanguage))
  .init({
    fallbackLng: ['ru', 'ky', 'en'],
    ns: [
      'app',
      'order',
      'auth',
    ],
    language: 'ru',
    defaultNS: 'app',
    fallbackNS: 'app',
    debug: DEBUG_MODE,
    resources: {
      ky: {
        app: require(`../locales/ky/app`),
        order: require(`../locales/ky/order`),
        auth: require(`../locales/ky/auth`),
      },
      en: {
        app: require(`../locales/en/app`),
        order: require(`../locales/en/order`),
        auth: require(`../locales/en/auth`),
      },
      ru: {
        app: require(`../locales/ru/app`),
        order: require(`../locales/ru/order`),
        auth: require(`../locales/ru/auth`),
      },
      uz: {
        app: require(`../locales/uz/app`),
        order: require(`../locales/uz/order`),
        auth: require(`../locales/uz/auth`),
      },
    },
  }, (err) => {
    if (err) {
      return console.log('something went wrong loading', err);
    }
  });

export default i18next;
