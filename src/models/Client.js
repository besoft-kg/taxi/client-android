import {types as t} from "mobx-state-tree";

export default t
  .model('client',{
    id: t.identifierNumber,
    phone_number: t.string,
    full_name: t.string,
    contacts: t.maybeNull(t.array(t.frozen())),
    last_action: t.Date,
    created_at: t.Date,
    updated_at: t.Date,
  }).views(self => ({
    get two_letter_name() {
      return (self.full_name.slice(0, 1) + self.full_name.slice(0, 1)).toUpperCase();
    },
  }));
