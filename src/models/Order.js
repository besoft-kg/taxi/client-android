import {getRoot, types as t} from 'mobx-state-tree';
import Category from './Category';
import Admin from './Admin';
import Client from './Client';
import float from './float';
import Driver from './Driver';
import React from 'react';
import Taximeter from './Taximeter';

export default t
  .model('order', {
    id: t.identifierNumber,
    publisher_id: t.maybeNull(t.integer),
    publisher: t.maybeNull(t.reference(Admin)),
    client_id: t.maybeNull(t.integer),
    client: t.maybeNull(t.reference(Client)),
    category_id: t.integer,
    category: t.maybeNull(t.reference(Category)),
    phone_number: t.maybeNull(t.string),
    status: t.enumeration([
      'pending',
      'fake_call',
      'fake_call_checking',
      'cancelled_by_client',
      'cancelled_by_admin',
      'going_to_client',
      'in_place',
      'going_with_client',
      'completed',
    ]),
    note: t.maybeNull(t.string),
    history: t.maybeNull(t.array(t.frozen())),
    payload: t.maybeNull(t.frozen()),
    address: t.maybeNull(t.string),
    address_lat: t.maybeNull(float),
    address_lng: t.maybeNull(float),
    created_at: t.Date,
    updated_at: t.Date,
    executor: t.maybeNull(t.reference(Driver)),
    executor_id: t.maybeNull(t.integer),
    // duration: t.maybeNull(t.integer),
    // duration_text: t.maybeNull(t.string),

    taximeter: t.maybeNull(t.reference(Taximeter)),
    fully_completed: t.optional(t.boolean, false),
  }).views(self => ({

    get root() {
      return getRoot(self);
    },

    get cancelled() {
      return self.status.startsWith('cancelled_by_');
    },

  })).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    return {
      setValue,
    };

  });
