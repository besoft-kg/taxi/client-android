import {types as t} from 'mobx-state-tree';
import float from './float';

export default t
  .model('taximeter', {
    id: t.identifierNumber,
    order_id: t.maybeNull(t.integer),
    driver_id: t.integer,
    started_at: t.Date,
    ended_at: t.maybeNull(t.Date),
    amount_to_pay: t.integer,
    duration_in_seconds: t.optional(t.integer, 0),
    waiting_time_in_seconds: t.optional(t.integer, 0),
    distance_in_m: t.optional(t.integer, 0),
    amount_info: t.frozen(),
    locations: t.array(t.frozen({
      lat: float,
      lng: float,
    })),
    created_at: t.Date,
    updated_at: t.Date,

    _waiting_interval_id: t.maybeNull(t.integer),
    fully_completed: t.optional(t.boolean, false),
  }).views(self => ({

    get ui_distance() {
      if (self.distance_in_m < 1) {
        return '0.0 км';
      }
      return `${(self.distance_in_m / 1000).toFixed(1)} км`;
    },

    get ui_waiting_time() {
      const minutes = Math.floor(self.waiting_time_in_seconds / 60) > 9 ? Math.floor(self.waiting_time_in_seconds / 60) : `0${Math.floor(self.waiting_time_in_seconds / 60)}`;
      const seconds = self.waiting_time_in_seconds % 60 > 9 ? self.waiting_time_in_seconds % 60 : `0${self.waiting_time_in_seconds % 60}`;
      return `${minutes}:${seconds}`;
    },

  })).actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    return {
      setValue,
    };

  });
