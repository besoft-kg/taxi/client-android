import {getRoot, types as t} from 'mobx-state-tree';
import Taximeter from '../models/Taximeter';
import {values} from 'mobx';

export default t
  .model('TaximeterStore', {

    items: t.optional(t.map(Taximeter), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.items.set(item.id, {
        ...item,
        started_at: new Date(item.started_at),
        ended_at: item.ended_at ? new Date(item.ended_at) : null,
        created_at: new Date(item.created_at),
        updated_at: new Date(item.updated_at),
      });
    };

    return {
      setValue,
      createOrUpdate,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

    get active_item() {
      return values(self.items).find(v => {
        if (v.ended_at) return !v.fully_completed;

        return v.ended_at === null;
      });
    },

  }));
