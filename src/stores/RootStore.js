import {types as t} from 'mobx-state-tree';
import AppStore from './AppStore';
import DriverStore from "./DriverStore";
import CategoryStore from './CategoryStore';
import OrderStore from './OrderStore';
import ClientStore from './ClientStore';
import TaximeterStore from './TaximeterStore';

export default t
  .model('RootStore', {

    appStore: AppStore,
    driverStore: DriverStore,
    categoryStore: CategoryStore,
    orderStore: OrderStore,
    clientStore: ClientStore,
    taximeterStore: TaximeterStore,

  })
  .actions(self => {

    const setDataFromStorage = (values) => {
      if (values[0]) self.appStore.token = values[0];
      if (values[1]) {
        self.clientStore.createOrUpdate(values[1]);
        self.appStore.user = values[1].id;
      }
      if (values[2]) self.appStore.fcm_token = values[2];
      self.categoryStore.createOrUpdate(Object.values(values[3]));
      self.appStore.setLanguage(values[4]);
      self.appStore.app_is_ready = true;
    };

    return {
      setDataFromStorage,
    };

  })
  .create({

    appStore: AppStore.create(),
    driverStore: DriverStore.create(),
    categoryStore: CategoryStore.create(),
    orderStore: OrderStore.create(),
    clientStore: ClientStore.create(),
    taximeterStore: TaximeterStore.create(),

  });
