import {getRoot, types as t} from 'mobx-state-tree';
import Driver from "../models/Driver";
import Image from '../models/Image';

export default t
  .model('DriverStore', {

    items: t.optional(t.map(Driver), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      self.items.set(item.id, {
        ...item,
        last_action: new Date(item.last_action),
        created_at: new Date(item.created_at),
        updated_at: new Date(item.updated_at),
        confirmed_at: item.confirmed_at ? new Date(item.confirmed_at) : null,
        location_last_refresh: item.location_last_refresh ? new Date(item.location_last_refresh) : null,
        drivers_license_expiration_date: item.drivers_license_expiration_date ? new Date(item.drivers_license_expiration_date) : null,

        picture: item.picture ? Image.create({
          ...item.picture,
          created_at: new Date(item.picture.created_at),
          updated_at: new Date(item.picture.updated_at),
        }) : null,

        datasheet_picture: item.datasheet_picture ? Image.create({
          ...item.datasheet_picture,
          created_at: new Date(item.datasheet_picture.created_at),
          updated_at: new Date(item.datasheet_picture.updated_at),
        }) : null,

        drivers_license_picture: item.drivers_license_picture ? Image.create({
          ...item.drivers_license_picture,
          created_at: new Date(item.drivers_license_picture.created_at),
          updated_at: new Date(item.drivers_license_picture.updated_at),
        }) : null,

        vehicle_picture: item.vehicle_picture ? Image.create({
          ...item.vehicle_picture,
          created_at: new Date(item.vehicle_picture.created_at),
          updated_at: new Date(item.vehicle_picture.updated_at),
        }) : null,
      });
    };

    return {
      setValue,
      createOrUpdate,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

  }));
