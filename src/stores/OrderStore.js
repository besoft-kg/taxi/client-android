import {getRoot, types as t} from 'mobx-state-tree';
import Order from '../models/Order';
import {values} from 'mobx';

export default t
  .model('OrderStore', {

    items: t.optional(t.map(Order), {}),

  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const remove = (id) => {
      self.items.delete(id);
    };

    const createOrUpdate = (item) => {
      if (Array.isArray(item)) {
        item.map(v => self.createOrUpdate(v));
        return;
      }

      if (item.taximeter) {
        self.root.taximeterStore.createOrUpdate(item.taximeter);
      }

      if (item.executor) {
        self.root.driverStore.createOrUpdate(item.executor);
      }

      if (item.client) {
        self.root.clientStore.createOrUpdate(item.client);
      }

      self.items.set(item.id, {
        ...item,
        taximeter: item.taximeter ? item.taximeter.id : null,
        executor: item.executor ? item.executor.id : null,
        client: item.client ? item.client.id : null,
        category: self.root.categoryStore.items.get(item.category_id),
        created_at: new Date(item.created_at),
        updated_at: new Date(item.updated_at),
      });
    };

    return {
      setValue,
      createOrUpdate,
      remove,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

    get active_item() {
      return values(self.items).find(v => {
        if (v.status === 'completed') {
          return !v.fully_completed;
        }

        return v.status !== 'fake_call' && !v.cancelled &&
          v.client_id === self.root.appStore.user.id;
      });
    },

  }));
