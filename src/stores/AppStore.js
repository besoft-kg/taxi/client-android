import {getRoot, getSnapshot, types as t} from 'mobx-state-tree';
import Client from '../models/Client';
import storage from '../utils/storage';
import requester from '../utils/requester';
import float from '../models/float';
import i18next from 'i18next';

const fetchAddress = (lat, lng, lang, callback) => {
  requester.post('/client/user/geocode', {
    lat, lng,
    language: lang,
  }).then((response) => {
    switch (response.status) {
      case 'success':
        callback(response.payload);
        break;
    }
  }).catch(e => {
    console.log(e);
  });
};

export default t
  .model('AppStore', {
    user: t.maybeNull(t.reference(Client)),
    token: t.maybeNull(t.string),
    fcm_token: t.maybeNull(t.string),
    auth_checking: t.optional(t.boolean, false),
    app_is_ready: t.optional(t.boolean, false),
    location_lat: t.maybeNull(float),
    location_lng: t.maybeNull(float),
    address: t.maybeNull(t.string),
    language: t.optional(t.enumeration(['ky', 'ru', 'en', 'uz']), 'ky'),
  })
  .actions(self => {

    const setValue = (name, value) => {
      self[name] = value;
    };

    const makeAuth = ({user, token, categories}) => {
      storage.set('token', token);
      self.root.clientStore.createOrUpdate(user);
      self.root.categoryStore.createOrUpdate(categories);
      self.user = user.id;
      storage.set('client', getSnapshot(self.user));
      storage.set('categories', getSnapshot(self.root.categoryStore.items));
      self.token = token;

      fetchAddress(self.location_lat, self.location_lng, self.language, (r) => {
        self.setAddress(r);
      });
    };

    const clearUser = () => {
      self.user = null;
      self.token = null;
    };

    const checkAuth = () => {
      if (!self.token || self.auth_checking) {
        return;
      }
      self.setValue('auth_checking', true);

      requester.get('/client/user/basic').then((response) => {
        self.root.clientStore.createOrUpdate(response.payload.user);
        self.root.categoryStore.createOrUpdate(response.payload.categories);
        if (response.payload.order) self.root.orderStore.createOrUpdate(response.payload.order);

        self.setValue('user', response.payload.user.id);
        storage.set('client', getSnapshot(self.user));
        storage.set('categories', getSnapshot(self.root.categoryStore.items));
      }).catch(e => {
        console.log(e);
        self.clearUser();
      }).finally(() => {
        self.setValue('auth_checking', false);
      });
    };

    const setLocation = (lat, lng) => {
      self.location_lat = lat;
      self.location_lng = lng;

      if (self.user) {
        fetchAddress(lat, lng, self.language, (r) => {
          self.setAddress(r);
        });
      }
    };

    const setAddress = (address) => {
      self.address = address.address;
      self.location_lat = address.lat;
      self.location_lng = address.lng;
    };

    const signOut = async () => {
      self.user = null;
      self.token = null;
      await storage.remove('client');
      await storage.remove('token');
    };

    const setBusy = (busy) => {
      if (self.authenticated && self.user) {
        self.user.setValue('busy', busy);
      }
    };

    const setLanguage = (lng) => {
      self.language = lng;
      i18next.changeLanguage(lng).then(() => {
      });
      storage.set('language', lng).then(() => {
      });

      if (self.location_lat && self.location_lng && self.user) {
        fetchAddress(self.location_lat, self.location_lng, lng, (r) => {
          self.setAddress(r);
        });
      }
    };

    return {
      signOut,
      setValue,
      makeAuth,
      checkAuth,
      clearUser,
      setLocation,
      setBusy,
      setLanguage,
      setAddress,
    };

  })
  .views(self => ({

    get root() {
      return getRoot(self);
    },

    get authenticated() {
      return self.user !== null;
    },

  }));
